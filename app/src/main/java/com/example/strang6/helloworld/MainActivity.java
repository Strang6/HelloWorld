package com.example.strang6.helloworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView HelloLenin;
    private TextView HelloYou;
    private EditText Who;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        HelloLenin = (TextView) findViewById(R.id.textView);
        HelloYou = (TextView) findViewById(R.id.textView2);
        Who = (EditText) findViewById(R.id.editText);
    }



    public void onClick(View view) {
        if (Who.getText().length()==0)
            HelloLenin.setText("Hello Lenin!");
        else
            HelloYou.setText("Здорова, " + Who.getText() + "!");
    }
}
